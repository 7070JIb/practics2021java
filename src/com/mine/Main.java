package com.mine;


import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.FileHandler;


public class Main {

    // Головная программа
    public static void main(String[] args) {
        // Список студентов
        List<Student> array = new ArrayList<Student>();
        // Если файл input.txt существует, то считать его, иначе - выйти из программы
        try(FileReader reader = new FileReader("input.txt")) {
            BufferedReader rdr = new BufferedReader(reader);
            // Считать каждую строку и спарсить в объект Student
            while(true) {
                String line = rdr.readLine();
                if(line == null) break;
                line = line.replace(" ", "");
                String [] pieces =  line.split(",");
                array.add(new Student(Integer.parseInt(pieces[3]),
                        Integer.parseInt(pieces[4]),
                        Integer.parseInt(pieces[5]),
                        pieces[0],
                        Integer.parseInt(pieces[1]),
                        Integer.parseInt(pieces[2])) );
            }
        }
        catch(IOException e)
        {
            System.out.println("Файла text.txt не существует");
            return;
        }
        // Вывести неотсортированный список в консоль
        for (int i = 0; i < array.size(); i++) {
            System.out.println(array.get(i) + " ");
        }

        // Привести к Comparable
        Comparable[] studentsArray = new ArrayList<Comparable>(array).toArray(new Comparable[array.size()]);

        // Отсортиовать
        if (array.size() >= 2) {
            MergeSort.comparator = new StudentsCompare<Student>();
            MergeSort.mergeSort(studentsArray);
        }

        System.out.println();
        System.out.println("Отсортированный: ");
        for (int i = 0; i < studentsArray.length; i++) {
            System.out.println(studentsArray[i] + " ");
        }

        // Вывод в файл output.txt
        boolean IsOutputReaded = false;
        while(!IsOutputReaded) {
            try (FileWriter writer = new FileWriter("output.txt")) {
                IsOutputReaded = true;
                writer.write("Список учеников, отсортированных в порядке возрастания средней оценки: \n" );
                for (int i = 0; i < studentsArray.length; i++) {
                    writer.write(i+": " + ((Student)studentsArray[i]).toString() + "\n");
                }
            } catch (IOException e) {
                try{ new File("output.txt").createNewFile();}
                    catch (IOException e2){}
            }
        }


    }

}

/**
 * Класс сортировки слиянием
 */
// Класс сортировки слияением
class MergeSort {
    // Выбранный компаратор
    /**
     * Компаратор двух объектов в массиве
     */
    public static Comparator comparator;

    /**
     * @param a массив сортируемых объектов
     */
    // Метод разбиения на два подмассива, и слияния. a - исходный массив, n - количество элементов
    public static void mergeSort(Comparable[] a) {
        // Если элементов меньше двух, то массив отсортирован
        if (a.length < 2) {
            return;
        }
        // Массив разбивается посередине на l - левый и r - правый
        int mid = a.length / 2;
        Comparable[] l = new Comparable[mid];
        Comparable[] r = new Comparable[a.length - mid];
        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < a.length; i++) {
            r[i - mid] = a[i];
        }
        // Подмассивы рекурсивно разбиваются на элементарные части и сортируются
        mergeSort(l);
        mergeSort(r);

        // Вызывается метод, который объединяет два отсортированных подмассива так, чтобы получился один отсортированный массив
        merge(a, l, r, mid, a.length - mid);
    }

    // Метод слияния двух отсортированных подмассивов в один массив. a - массив, в который все записывается.
    // l - левый подмассив, r - правый, left, right - кол-во элементов в соотв. подмассивах
    private static void merge(
            Comparable[] a, Comparable[] l, Comparable[] r, int left, int right) {

        // i - индекс левого подмассива, j - правого, k - основного
        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            int comparison = comparator.compare(l[i], r[j]);
            // Следующим элементом массива a записывается больший из двух рассматриваемых элементов подмассивов
            if (comparison == -1) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }

        }
        // Как только один из подмассивов полностью записан, в основной массив дописываеются
        // оставшиеся элементы другого подмассива.
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }
}


/**
 * Класс, представляющий абстрактного человека
 */
abstract class Human implements Comparable
{
    // В нем определены поля года рождения, имени, роста
    final protected int _birthYear;
    final protected String _name;
    final protected int _height;

    /**
     * @param name имя
     * @param birthYear год рождения
     * @param height рост в см
     */
    Human(String name, int birthYear,  int height){
        _birthYear = birthYear;
        _name = name;
        _height = height;
    }


    /**
     * @return возраст в годах
     */
    public int get_age(){
            return new GregorianCalendar().get(Calendar.YEAR) - _birthYear;
    }


    /**
     * @return год рождения
     */
    public int get_birthYear() {
        return _birthYear;
    }

    /**
     * @return рост в см
     */
    public int get_height() {
        return _height;
    }

    /**
     * @return имя
     */
    public String get_name() {
        return _name;
    }

    /**
     * @return строковое представление человека
     */
    // Строковое представление объекта
    @Override
    public String toString(){
        return _name+", " + get_age() + " лет, " + _height + " см.";
    }



    /**
     * @param o второй человек для сравнения
     * @return 1, если второй человек ниже или если они одного роста, но первый младше. Иначе -1
     */
    // Метод сравнивает человека с другим
    // в первую очередь по росту,
    // во вторую - по году рождения
    @Override
    public int compareTo(Object o) {
        Human other = (Human) o;
        if(this.get_height() > other.get_height()) return 1;
        else if(this.get_height() < other.get_height()) return -1;
        else if(this.get_birthYear() < other.get_birthYear()) return 1;
        return -1;
    }

    /**
     * @param obj второй человек для сравнения
     * @return true, если равны
     */
    // Чтобы два человека считались равными,
    // у них должны быть одинаковые значения 
    // всех полей
    @Override
    public boolean equals(Object obj) {

        if (obj == null || getClass() != obj.getClass()) return false;

        Human other = (Human) obj;

        return (other.get_height() == this.get_height()
        && other.get_age() == this.get_age()
        && other.get_name() == this.get_name());
    }

    /**
     * @return хэщ-код объекта человека
     */
    // Хэш-код объекта = хэш-код
    // строкового представления
    @Override
    public int hashCode(){
        return toString().hashCode();
    }
}

/**
 * Класс студента
 */
class Student extends Human
{
    // Поля : оценка по математике, по физике, по программированию
    final private int _mathMark;
    final private int _physicsMark;
    final private int _computerScienceMark;

    /**
     * @param mathMark оценка по математике
     * @param physicsMark оценка по физике
     * @param computerScienceMark оценка по программированию
     * @param name имя
     * @param birthYear год рождения
     * @param height рост в см
     */
    Student(int mathMark, int physicsMark, int computerScienceMark, String name, int birthYear,  int height) {
        super(name, birthYear, height);
        _mathMark = mathMark;
        _physicsMark = physicsMark;
        _computerScienceMark = computerScienceMark;
    }


    /**
     * @return средний балл ученика
     */
    // Возвращает средний балл ученика
    public float get_average_mark(){
        return (_mathMark + _physicsMark + _computerScienceMark) / 3f;
    }

    /**
     * @return оценка по программированию
     */
    public int get_computerScienceMark() {
        return _computerScienceMark;
    }

    /**
     * @return оценка по математике
     */
    public int get_mathMark() {
        return _mathMark;
    }

    /**
     * @return оценка по физике
     */
    public int get_physicsMark() {
        return _physicsMark;
    }

    /**
     * @return строоковое представление студента
     */
    @Override
     public String toString(){
        return super.toString()
                + " Оценки: \"математика\" - " + _mathMark
                + ", \"физика\" - " + _physicsMark
                + ", \"программирование\" - " + _computerScienceMark
                + ". Средняя оценка - " + get_average_mark();
    }

    /**
     * @param o второй студент для сравнения
     * @return 1, если у второго студента срдний балл ниже или равен, но ниже оценка по программированию. Иначе -1
     */
    // Ученики сравниваются в первую очередь по среднему баллу,
    // во вторую - по оценке по программированию
    public int compareTo(Object o) {

        Student other = (Student) o;
        if(this.get_average_mark() > other.get_average_mark()) return 1;
        else if(this.get_average_mark() < other.get_average_mark()) return -1;
        else if(this._computerScienceMark > other._computerScienceMark) return 1;
        return -1;
    }

    /**
     * @param obj второй студент для сравнения
     * @return true, если равны
     */
    // Студенты равны, если у них одинаковые поля
    // в родительском классе Human, и одинаковые оценки
    @Override
    public boolean equals(Object obj) {
        Student other = (Student) obj;
        return (super.equals(obj)
        && _physicsMark == other.get_physicsMark()
        &&_mathMark == other.get_mathMark()
        && _computerScienceMark == other.get_computerScienceMark());
    }

    /**
     * @return хэщ-код студента
     */
    @Override
    public int hashCode(){
        return toString().hashCode();
    }

}

/**
 * @param <T> Компаратор студентов
 */
// Класс для сравнения двух студентов
class StudentsCompare<T extends  Student> implements Comparator<T>{

    /**
     * @param o1 студент 1
     * @param o2 студент 2
     * @return 1, если у второго студента срдний балл ниже или равен, но ниже оценка по программированию. Иначе -1
     */
    @Override
    public int compare(T o1,T o2) {
        if(o1 == null || o2 == null) return 0;

        return o1.compareTo(o2);

    }
}

/**
 * @param <T> Компаратор людей
 */
// Класс для сравнения двух людей
class HumanCompare<T extends  Human> implements Comparator<T> {

    /**
     * @param o1 человек 1
     * @param o2 человек 2
     * @return 1, если второй человек ниже или если они одного роста, но первый младше. Иначе -1
     */
    @Override
    public int compare(T o1, T o2) {
        if(o1 == null || o2 == null) return 0;

        return  o1.compareTo(o2);
    }
}
